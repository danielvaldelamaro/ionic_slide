import { Component } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage {


  constructor() {}

  slideOpt = {
    initialSlide: 0, 
    slidesPerView: 1, 
    centerSlides: true, 
    speed: 400 
  }

  slides = [
    {
      title: "BeSoccer",
      subtitle: "Horarios de partidos",
      img: '../../assets/images/img_3.jpeg',
      text: "Consulta los horarios de tus ligas y equipos favoritos"
    },
    {
      title: "BeSoccer",
      subtitle: "Estadistica de Partidos",
      img: '../../assets/images/img_2.jpeg',
      text: "Visualiza las estadisticas de partidos en tiempo real"
    },
    {
      title: "BeSoccer",
      subtitle: "Predicción de resultados",
      img: '../../assets/images/img_1.jpeg',
      text: "Consulta la prediccion de resultados para realizar apuestas con tus equipos favoritos"
    }
  ]
}
